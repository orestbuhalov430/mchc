# coding: utf-8
from __future__ import unicode_literals

from py2neo import neo4j
from py2neo.neo4j import cypher
from faker import Factory
from random import choice

from django.utils.timezone import now

graph_db = neo4j.GraphDatabaseService()


create_qs = "START n=node({message_id}) " \
                 "CREATE (n)-[r:LINK_TO]->(m {{text: '{message}', datetime_created:{datetime_created}}}) " \
                 "RETURN id(m)"

get_ids_qs = "START n=node(*) RETURN id(n)"

#from scripts_local.fill_data import main
def main():
    fake = Factory.create()
    # создаем начальную тыщу сообщений
    for i in xrange(1000):
        if i % 100 == 0:
            print i
        kwargs = {
            'message_id': 1,
            'message': fake.text(),
            'datetime_created': float(now().strftime('%s.%f'))
        }
        result, metadata = cypher.Query(graph_db, create_qs.format(**kwargs)).execute()
    result, metadata = cypher.Query(graph_db, get_ids_qs).execute()
    IDS = [i[0] for i in result]
    for i in xrange(10**6):
        if i % 1000 == 0:
            print i
        parent = choice(IDS)
        kwargs = {
            'message_id': parent,
            'message': fake.text(),
            'datetime_created': float(now().strftime('%s.%f'))
        }
        result, metadata = cypher.Query(graph_db, create_qs.format(**kwargs)).execute()
        IDS.append(result[0][0])
    print "Done"

if __name__ == "__main__":
    main()
