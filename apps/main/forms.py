# coding: utf-8
from __future__ import unicode_literals

from django import forms
from django.conf import settings
from django.core.mail import send_mail
from django.forms.models import inlineformset_factory, modelformset_factory
from django.utils.translation import ugettext_lazy as _


from main.models import *


class ScheduleModelForm(forms.ModelForm):
    class Meta:
        model = OperationSchedule
        exclude = ('pdf_file',)


OperatingRoomFormset = inlineformset_factory(
    OperationSchedule,
    OperatingRoom,
    extra=0,
    max_num=5,
    can_delete=False
)


OperationFormset = modelformset_factory(
    Operation,
    extra=0,
    max_num=10,
    can_delete=False
)


OperationStaffFormset = modelformset_factory(
    OperationStaff,
    extra=0,
    max_num=10,
    can_delete=False
)


ManipulationFormset = inlineformset_factory(
    OperationSchedule,
    Manipulation,
    extra=0,
    max_num=10,
    can_delete=False
)
