# coding: utf-8
from __future__ import unicode_literals

from django.contrib import admin

from main.models import OperationSchedule, OperatingRoom, Diagnosis, Surgeons, Operation, OperationKinds


admin.site.register(OperationSchedule)
admin.site.register(OperatingRoom)
admin.site.register(Diagnosis)
admin.site.register(Surgeons)
admin.site.register(Operation)
admin.site.register(OperationKinds)
