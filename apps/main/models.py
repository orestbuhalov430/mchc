# coding: utf-8
from __future__ import unicode_literals

from datetime import date, timedelta, time

from django.db import models
from django.forms.models import inlineformset_factory
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class OperationSchedule(models.Model):
    date = models.DateField('График операций на ', default=date.today() + timedelta(days=1))
    pdf_file = models.FileField('Pdf версия', upload_to='/schedule/%Y/%m/%d', blank=True, null=True)

    def askmkfs(self):
        return self.operatingroom_set

    class Meta:
        ordering = ('-id',)
        verbose_name = 'График операций'
        verbose_name_plural = 'Графики операций'

    def __str__(self):
        return str(self.date)


@python_2_unicode_compatible
class OperatingRoom(models.Model):
    schedule = models.ForeignKey(OperationSchedule)
    num = models.IntegerField('Операционная №')
    start_time = models.TimeField('Время начала операций', default=time(hour=9, minute=30))

    class Meta:
        verbose_name = 'Операционная'
        verbose_name_plural = 'Операционные'

    def __str__(self):
        return str(self.num) + ' стол'


OPERATION_KIND_CHOICES = (
    ('o', 'открытая'),
    ('l', 'лапароскопическая'),
)


@python_2_unicode_compatible
class Diagnosis(models.Model):
    name = models.CharField('Диагноз', max_length=500)

    class Meta:
        verbose_name = 'Диагноз'
        verbose_name_plural = 'Диагноз'

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Surgeons(models.Model):
    name = models.CharField('ФИО хирурга', max_length=500)
    is_operator = models.BooleanField('Может быть оператором.', default=True)

    class Meta:
        verbose_name = 'Хирург'
        verbose_name_plural = 'Хирурги'

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class OperationKinds(models.Model):
    name = models.CharField('название операции', max_length=500)

    class Meta:
        verbose_name = 'название операции'
        verbose_name_plural = 'названия операции'

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Operation(models.Model):
    room = models.ForeignKey(OperatingRoom)
    ward = models.IntegerField('Палата №', blank=True, null=True)
    code = models.CharField('code', max_length=255)
    patient = models.CharField('ФИО пациента', max_length=500)
    diagnosis = models.ForeignKey(Diagnosis, verbose_name='Диагноз')
    kind = models.ForeignKey(OperationKinds, verbose_name='Название')

    class Meta:
        verbose_name = 'Операция'
        verbose_name_plural = 'Операция'

    def __str__(self):
        return self.patient

    @property
    def ward_display(self):
        if self.ward:
            return unicode(self.ward) + ' п.'
        return '--'


STAFF_ROLE_CHOICES = (
    ('a', 'acc.'),
    ('o', 'опер.')
)


@python_2_unicode_compatible
class OperationStaff(models.Model):
    role = models.CharField('роль', max_length=1, choices=STAFF_ROLE_CHOICES, default='a')
    code = models.CharField('code', max_length=255)
    operation = models.ForeignKey(Operation, blank=True, null=True)
    surgeon = models.ForeignKey(Surgeons, verbose_name='Хирург')

    class Meta:
        verbose_name = 'Операционная бригада'
        verbose_name_plural = 'Операционная бригада'

    def __str__(self):
        return self.get_role_display() + ' ' + self.surgeon.name


@python_2_unicode_compatible
class ManipulationKinds(models.Model):
    name = models.CharField('Название манипуляции', max_length=500)

    class Meta:
        verbose_name = 'Название манипуляции'
        verbose_name_plural = 'Названия манипуляций'

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Manipulation(models.Model):
    schedule = models.ForeignKey(OperationSchedule)
    ward = models.IntegerField('Палата №', blank=True, null=True)
    patient = models.CharField('ФИО пациента', max_length=500)
    manipulation_type = models.ForeignKey(ManipulationKinds, verbose_name='Название манипуляции')
    surgeon = models.ForeignKey(Surgeons, verbose_name='Хирург', blank=True, null=True)
    start_time = models.TimeField('время', default=time(hour=9, minute=30), blank=True, null=True)

    class Meta:
        verbose_name = 'Манипуляция'
        verbose_name_plural = 'Манипуляции'

    def __str__(self):
        return self.patient

    @property
    def ward_display(self):
        if self.ward:
            return unicode(self.ward) + ' п.'
        return '--'
