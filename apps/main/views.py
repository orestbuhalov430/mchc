# coding: utf-8
from __future__ import unicode_literals

import json
from django.views.generic import ListView
from django.shortcuts import render, Http404, HttpResponse, redirect

from helpers import make_pdf
from main.forms import ScheduleModelForm, OperatingRoomFormset, OperationFormset, OperationStaffFormset, ManipulationFormset
from main.models import Operation, OperationSchedule, OperatingRoom, Diagnosis, Surgeons, OperationStaff, OperationKinds, Manipulation, ManipulationKinds


class OperationScheduleListView(ListView):
    paginate_by = 100
    model = OperationSchedule

    def get_queryset(self, *args, **kwargs):
        qs = super(OperationScheduleListView, self).get_queryset(*args, **kwargs)
        for sched in OperationSchedule.objects.prefetch_related('operatingroom_set', 'operatingroom_set__operation_set').all():
            res = 0
            for room in sched.operatingroom_set.all():
                res += len(list(room.operation_set.all()))
            if not res:
                sched.delete()
        return qs


def schedule_add(request, schedule_id=None):
    if request.method == "POST" and schedule_id:
        schedule = OperationSchedule.objects.get(id=schedule_id)
        schedule_form = ScheduleModelForm(request.POST, instance=schedule)
        if schedule_form.is_valid():
            schedule = schedule_form.save()
        rooms_formset = OperatingRoomFormset(request.POST, instance=schedule)
        if rooms_formset.is_valid():
            rooms_formset.save()
        manipulations = ManipulationFormset(request.POST,instance=schedule, prefix="manipulationformset")
        if manipulations.is_valid():
            manipulations.save()
        operations_formset = OperationFormset(request.POST, prefix="operationformset", queryset=Operation.objects.none())
        if operations_formset.is_valid():
            operations = operations_formset.save()
            operations_dict = dict([(i.code, i) for i in operations])
            operation_staff_formset = OperationStaffFormset(request.POST, prefix="operationstaffformset",queryset=OperationStaff.objects.none())
            if operation_staff_formset.is_valid():
                operation_staff = operation_staff_formset.save(commit=False)
                for opstff in operation_staff:
                    opstff.operation = operations_dict.get(opstff.code)
                    opstff.save()
                return redirect('schedule-preview', schedule_id=schedule.id)
        aaa = operations_formset.errors
        raise
    else:
        schedule = OperationSchedule.objects.create()
        schedule_form = ScheduleModelForm(instance=schedule)
        default_rooms = [
            OperatingRoom.objects.create(schedule=schedule, num=13),
            OperatingRoom.objects.create(schedule=schedule, num=14),
        ]
        rooms = OperatingRoomFormset(instance=schedule)
        operations = OperationFormset(prefix="operationformset", queryset=Operation.objects.none())
        operation_staff = OperationStaffFormset(prefix="operationstaffformset", queryset=OperationStaff.objects.none())
        manipulations = ManipulationFormset(prefix="manipulationformset", instance=schedule)
    return render(request, 'main/schedule_add.html', locals())


def schedule_preview(request, schedule_id):
    object = OperationSchedule.objects.prefetch_related(
        'operatingroom_set',
        'operatingroom_set__operation_set',
        'operatingroom_set__operation_set__diagnosis',
        'operatingroom_set__operation_set__operationstaff_set',
        'operatingroom_set__operation_set__operationstaff_set__surgeon'
    ).get(id=schedule_id)
    return render(request, 'main/schedule_preview.html', {'object': object})


def add_item(request):
    result = None
    if request.POST.get('new_kind'):
        result, created = OperationKinds.objects.get_or_create(name=request.POST.get('new_kind'))
    if request.POST.get('new_diagnosis'):
        result, created = Diagnosis.objects.get_or_create(name=request.POST.get('new_diagnosis'))
    if request.POST.get('new_surgeon'):
        result, created = Surgeons.objects.get_or_create(name=request.POST.get('new_surgeon'))
    if request.POST.get('new_manipulation'):
        result, created = ManipulationKinds.objects.get_or_create(name=request.POST.get('new_manipulation'))
    if not result is None:
        return HttpResponse(
            json.dumps({
                'data': '<option value="{id}">{name}</option>'.format(id=result.id, name=result.name),
                'id': result.id
            })
        )
    raise Http404


def schedule_pdf(request, schedule_id):
    object = OperationSchedule.objects.prefetch_related(
        'operatingroom_set',
        'operatingroom_set__operation_set',
        'operatingroom_set__operation_set__diagnosis',
        'operatingroom_set__operation_set__operationstaff_set',
        'operatingroom_set__operation_set__operationstaff_set__surgeon'
    ).get(id=schedule_id)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="schedule{date:-%d-%m-%Y}.pdf"'.format(date=object.date)

    return make_pdf(response, object)

