# coding: utf-8
from __future__ import unicode_literals
import os
import json
from django.conf import settings
from reportlab.lib.pagesizes import landscape, A4
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib import colors
from reportlab.lib.units import inch
from reportlab.platypus import Paragraph, Frame, Table, Spacer
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

f = open(settings.JSON_SETTINGS_FILE, 'rb')
JSON_SETTINGS = json.load(f)
f.close()

styles = getSampleStyleSheet()
PW, PH = landscape(A4)

# JSON_SETTINGS = {
#     'FS_DEFAULT': 12,
#     'FS_TITLE': 20,
#     'FS_SURGEON': 12,
#     'MAX_LINE_LENGTH': 18,
#     'SYMBOL_SIZE': 10,
#     'FS_MIN': 8,
# }
#
# json.dump(JSON_SETTINGS, open(settings.JSON_SETTINGS_FILE, 'wb'), indent=4)


DEFAULT_FONT = 'MumXAT9N'
FS_DEFAULT = JSON_SETTINGS.get('FS_DEFAULT', 12)
FS_TITLE = JSON_SETTINGS.get('FS_TITLE', 20)
FS_SURGEON = JSON_SETTINGS.get('FS_SURGEON', FS_DEFAULT)
MAX_LINE_LENGTH = JSON_SETTINGS.get('MAX_LINE_LENGTH', (FS_DEFAULT - 3) * 2)
SYMBOL_SIZE = JSON_SETTINGS.get('SYMBOL_SIZE', 10)
FS_MIN = JSON_SETTINGS.get('FS_MIN', 9)

styleN = ParagraphStyle(
    name='Normal',
    fontName=DEFAULT_FONT,
    fontSize=FS_DEFAULT,
    leading=FS_DEFAULT,
    # borderWidth=0.1,
    # borderColor='red'
)

styleI = ParagraphStyle(
    name='LeftIndent',
    fontName=DEFAULT_FONT,
    fontSize=FS_DEFAULT,
    leading=FS_DEFAULT,
    # borderWidth=1,
    # borderColor='red',
    leftIndent=100,
)

styleH = styles['Heading3']

styleOPN = ParagraphStyle(
    name='OPN',
    fontName=DEFAULT_FONT,
    fontSize=FS_DEFAULT,
    leading=FS_DEFAULT,
    # borderWidth=0.1,
    # borderColor='red'
)


def get_opname_paragraph(lines):
    for num, i in enumerate(lines):
        if len(i) > MAX_LINE_LENGTH:
            lines[num] = i[:MAX_LINE_LENGTH] + '<br />' + i[MAX_LINE_LENGTH:]
        if num == 1:
            lines[num] = "<i>{line}</i>".format(line=lines[num])
    paragraph_xml = "<br />".join(lines)
    return Paragraph(paragraph_xml, styleOPN)


def get_max_width_para(line, width=100):
    line = unicode(line)
    actual_fs = FS_DEFAULT
    MAX_LINE_LENGTH = width/SYMBOL_SIZE
    if len(line) > MAX_LINE_LENGTH:
        k = 1.3
        actual_fs = round(k*width/len(line))
    print actual_fs, len(line)
    if actual_fs < FS_MIN:
        actual_fs = FS_MIN
    print actual_fs, len(line)
    actual_style = ParagraphStyle(
        name='ActualStyle',
        fontName=DEFAULT_FONT,
        fontSize=actual_fs,
        leading=actual_fs,
        # borderWidth=1,
        # borderColor='red'
    )
    return Paragraph(line, actual_style)


def get_operation_frame(operation, cvs):
    opname = operation.diagnosis.name.encode('utf-8').decode('utf-8')
    data = []
    num = 1
    for num, i in enumerate(operation.operationstaff_set.all()):
        cell_paragraph = get_opname_paragraph([opname, operation.kind.name])
        if num == 0:
            data.append([operation.ward_display, get_max_width_para(operation.patient), cell_paragraph, get_max_width_para(i, 120)])
        elif num == 1:
            data.append(['', '', '', get_max_width_para(i, 120)])
        else:
            data.append(['', '', '', get_max_width_para(i, 120)])
    t = Spacer(1, 0.2*inch)
    if data:
        t = Table(data, style=[
            ('FONT', (0, 0), (-1, -1), DEFAULT_FONT),
            ('FONTSIZE', (0, 0), (-1, -1), FS_DEFAULT),
            ('LINEABOVE', (0, 0), (-1, 0), 0.5, colors.black),
            ('TOPPADDING', (0, 0), (-1, -1), 0),
            # ('GRID', (0, 0), (-1, -1), 0.1, colors.red),
            ('SPAN', (2, 0), (-2, num)),
        ])
        t._argW[0] = 30
        t._argW[1] = 100
        t._argW[2] = 150
        t._argW[3] = 120
    return t


def get_manipulation_frame(manipulations):
    data = []
    for i in manipulations:
        surgeon_name = ''
        if i.surgeon:
            surgeon_name = i.surgeon.name
        if i.start_time:
            surgeon_name += i.start_time.strftime(" %H:%M")
        data.append([
            i.ward_display,
            get_max_width_para(i.patient),
            get_max_width_para(i.manipulation_type.name, 140),
            get_max_width_para(surgeon_name, 110)
        ])
    t = Table(data, style=[
        ('FONT', (0, 0), (-1, -1), DEFAULT_FONT),
        ('FONTSIZE', (0, 0), (-1, -1), FS_DEFAULT),
        ('LINEABOVE', (0, 0), (-1, 0), 0.5, colors.black),
        ('TOPPADDING', (0, 0), (-1, -1), 0),
        # ('GRID', (0, 0), (-1, -1), 0.1, colors.red),
    ])
    t._argW[0] = 30
    t._argW[1] = 100
    t._argW[2] = 150
    t._argW[3] = 120
    return t


def make_pdf(response, object):
    cvs = canvas.Canvas(response, pagesize=landscape(A4))
    pdfmetrics.registerFont(TTFont(DEFAULT_FONT, os.path.join(settings.STATIC_ROOT, 'fonts', DEFAULT_FONT + '.ttf')))
    cvs.setFont(DEFAULT_FONT, FS_TITLE)
    cvs.drawCentredString(PW/2.0, PH-FS_TITLE, object.date.strftime('%d.%m.%Y'))
    # cvs.drawCentredString(PW/8.0, PH-FS_TITLE, object.comment)
    storage = [[], []]
    for i, room in enumerate(object.operatingroom_set.all()):
        story = storage[i % 2]
        story.append(Spacer(1, 0.2*inch))
        story.append(Paragraph(unicode(room) + '  ' + room.start_time.strftime("%H:%M"), styleI))
        story.append(Spacer(1, 0.2*inch))
        for operation in room.operation_set.all():
            story.append(get_operation_frame(operation, cvs))
    manipulations = object.manipulation_set.all()
    if manipulations:
        storage[1].append(Spacer(1, 0.4*inch))
        storage[1].append(get_manipulation_frame(manipulations))
    left_column = Frame(0, PH/2.0 + 20, PW/2.0, PH/2.0 - 20)#, showBoundary=1)
    left_column.addFromList(storage[0], cvs)
    right_column = Frame(PW/2.0, PH/2.0 + 20, PW/2.0, PH/2.0-20)
    right_column.addFromList(storage[1], cvs)
    cvs.drawCentredString(PW/2.0, PH-FS_TITLE, object.date.strftime('%d.%m.%Y'))
    storage = [[], []]
    for i, room in enumerate(object.operatingroom_set.all()):
        story = storage[i % 2]
        story.append(Spacer(1, 0.2*inch))
        story.append(Paragraph(unicode(room) + '  ' + room.start_time.strftime("%H:%M"), styleI))
        story.append(Spacer(1, 0.2*inch))
        for operation in room.operation_set.all():
            story.append(get_operation_frame(operation, cvs))
    manipulations = object.manipulation_set.all()
    if manipulations:
        storage[1].append(Spacer(1, 0.4*inch))
        storage[1].append(get_manipulation_frame(manipulations))
    left_column1 = Frame(0, 0, PW/2.0, PH/2.0 - 10)
    left_column1.addFromList(storage[0], cvs)
    right_column1 = Frame(PW/2.0, 0, PW/2.0, PH/2.0-10)
    right_column1.addFromList(storage[1], cvs)
    cvs.drawCentredString(PW/2.0, PH/2.0-FS_TITLE, object.date.strftime('%d.%m.%Y'))
    cvs.save()
    return response
