#!/usr/bin/env bash

# Создаем симлинк на директорию проекта
echo "Symlink working directory to /var/www/mchc..."
ln -fs /vagrant /var/www/mchc

# Если нет виртуального окружения - создаем
if [ ! -d "/home/vagrant/.virtualenvs/mchc" ]; then
    echo "Create python virtual environment..."
    su vagrant -c "virtualenv --no-site-packages /home/vagrant/.virtualenvs/mchc/"
fi

# Устанавливаем необходимые пакеты через pip
echo "Install PIP packages..."
su vagrant -c "/home/vagrant/.virtualenvs/mchc/bin/pip install -r /var/www/mchc/requirements.txt"

# Создаем директорию для log-файлов
echo "Create logs directory..."
su vagrant -c "mkdir -p /var/www/mchc/logs"

# Создаем директорию для static-файлов
echo "Create static directory..."
su vagrant -c "mkdir -p /var/www/mchc/static"

# Создаем директорию для locale-файлов
echo "Create locale directory..."
su vagrant -c "mkdir -p /var/www/mchc/locale"

# Правим hosts
echo "Symlink /etc/hosts..."
rm /etc/hosts
ln -s /var/www/mchc/Vagrantfiles/etc/hosts /etc/hosts

# Nginx
echo "Configure & Restart nginx..."
rm /etc/nginx/conf.d/default.conf
ln -s /var/www/mchc/Vagrantfiles/etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
service nginx restart

# uWsgi
echo "Configure uWsgi..."
service uwsgi stop
if [ -f /etc/uwsgi/mchc.ini ]; then
    rm /etc/uwsgi/mchc.ini
fi
ln -s /var/www/mchc/Vagrantfiles/etc/uwsgi/mchc.ini /etc/uwsgi/mchc.ini
service uwsgi start

# Database
echo "Check DB..."
if ! su postgres -c "psql -d mchc -c\"\dn\""; then
    echo "Creating database & User..."
    su postgres -c "createdb mchc"
    su postgres -c "psql -c \"CREATE USER mchc WITH PASSWORD 'mchc' CREATEDB;\""
    su postgres -c "psql -d mchc -c \"CREATE SCHEMA AUTHORIZATION mchc;\""
fi

echo "Bootstrap finished!"
